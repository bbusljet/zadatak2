﻿using System.Threading.Tasks;

namespace Zadatak2.Services.Interfaces
{
    public interface IEmailService
    {
        Task SendEmailAsync(string to, string from, string message);
        Task SendContactFormEmailAsync(string from, string message);
        Task SendRegisterEmailAsync(string to);
        Task SendResetPasswordEmailAsync(string to, string message);
    }
}