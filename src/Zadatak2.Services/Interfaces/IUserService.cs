﻿using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;
using Zadatak2.Models;
using Zadatak2.Models.Models;

namespace Zadatak2.Services.Interfaces
{
    public interface IUserService
    {
        Task<IdentityResult> AddLoginAsync(User user, UserLoginInfo userLoginInfo);

        AuthenticationProperties ConfigureExternalAuthenticationProperties(string provider, string redirectUrl, string userId);

        Task<IdentityResult> CreateAsync(User user);

        Task<SignInResult> ExternalLoginSignInAsync(string loginProvider, string providerKey, bool isPersistent);

        Task<User> FindByEmailAsync(string email);

        Task<string> GeneratePasswordTokenAsync(User user);

        Task<ExternalLoginInfo> GetExternalLoginInfoAsync(string expectedXsrf);

        Task<List<User>> GetProfileAsync(string user);

        IEnumerable<User> GetRegistered();

        Task LogoffAsync();

        Task<SignInResult> PasswordSignInAsync(string username, string passwordHash, bool isPersistent, bool lockout);

        Task<IdentityResult> RegisterAsync(User user, string password);

        Task<IdentityResult> ResetPasswordAsync(User user, string code, string password);

        Task SignInAsync(User user, bool isPersistent, string authenticationMethod);
    }
}