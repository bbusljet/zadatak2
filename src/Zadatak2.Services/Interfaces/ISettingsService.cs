﻿using Zadatak2.Models;

namespace Zadatak2.Services
{
    public interface ISettingsService
    {
        EmailSettings GetMailSettings();
        string GetConsumerKey();
        string GetConsumerSecret();
    }
}
