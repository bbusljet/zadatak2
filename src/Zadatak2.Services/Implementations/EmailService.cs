﻿using MailKit.Net.Smtp;
using MimeKit;
using System.Threading.Tasks;
using Zadatak2.Models;
using Zadatak2.Services.Interfaces;

namespace Zadatak2.Services.Implementations
{
    public class EmailService : IEmailService
    {
        private ISettingsService _settingsService;
        private EmailSettings _emailConfiguration;

        public EmailService(ISettingsService settingsService)
        {
            _settingsService = settingsService;
            _emailConfiguration = settingsService.GetMailSettings();
        }

        public async Task SendEmailAsync(string to, string from, string message)
        {
            MimeMessage mimeMessage = new MimeMessage();
            mimeMessage.From.Add(new MailboxAddress(""));
            mimeMessage.To.Add(new MailboxAddress(to));
            mimeMessage.Body = new TextPart("plain") { Text = message };

            using (var client = new SmtpClient())
            {
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                await client.ConnectAsync(_emailConfiguration.smtpHost, _emailConfiguration.smtpPort, false);
                await client.AuthenticateAsync(_emailConfiguration.userName, _emailConfiguration.password);
                await client.SendAsync(mimeMessage);

                await client.DisconnectAsync(true);
            }
        }

        public async Task SendContactFormEmailAsync(string from, string message)
        {
            var adminMail = "postmaster@dragonzdomain.com";

            await SendEmailAsync(adminMail, from, message);
        }

        public async Task SendRegisterEmailAsync(string to)
        {
            var message = "You have registered succesfully";
            var from = "Admin";

            await SendEmailAsync(to, from, message);
        }

        public async Task SendResetPasswordEmailAsync(string to, string message)
        {
            var from = "Admin";

            await SendEmailAsync(to, from, message);
        }
    }
}