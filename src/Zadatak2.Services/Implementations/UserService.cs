﻿using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;
using Zadatak2.Models;
using Zadatak2.Repository;
using Zadatak2.Services.Interfaces;
using System;
using Zadatak2.Models.Models;

namespace Zadatak2.Services.Implementations
{
    public class UserService : IUserService
    {
        private IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<IdentityResult> AddLoginAsync(User user, UserLoginInfo userLoginInfo)
        {
            return await _userRepository.AddLoginAsync(user, userLoginInfo);
        }

        public AuthenticationProperties ConfigureExternalAuthenticationProperties(string provider, string redirectUrl, string userId)
        {
            var properties = _userRepository.ConfigureExternalAuthenticationProperties(provider, redirectUrl, userId);
            return properties;
        }

        public async Task<IdentityResult> CreateAsync(User user)
        {
            return await _userRepository.CreateAsync(user);
        }

        public async Task<SignInResult> ExternalLoginSignInAsync(string loginProvider, string providerKey, bool isPersistent)
        {
            return await _userRepository.ExternalLoginSignInAsync(loginProvider, providerKey, isPersistent);
        }

        public async Task<User> FindByEmailAsync(string email)
        {
            return await _userRepository.FindByEmailAsync(email);
        }

        public async Task<string> GeneratePasswordTokenAsync(User user)
        {
            return await _userRepository.GeneratePasswordTokenAsync(user);
        }

        public async Task<ExternalLoginInfo> GetExternalLoginInfoAsync(string expectedXsrf)
        {
            return await _userRepository.GetExternalLoginInfoAsync(expectedXsrf);
        }

        public async Task<List<User>> GetProfileAsync(string user)
        {
            return await _userRepository.GetProfileAsync(user);
            
        }

        public IEnumerable<User> GetRegistered()
        {
            return _userRepository.GetRegistered();
        }

        public async Task LogoffAsync()
        {
            await _userRepository.LogoffAsync();
        }

        public async Task<SignInResult> PasswordSignInAsync(string username, string passwordHash, bool isPersistent, bool lockout)
        {
            return await _userRepository.PasswordSignInAsync(username, passwordHash, isPersistent, lockout);
        }

        public async Task<IdentityResult> RegisterAsync(User user, string password)
        {
            return await _userRepository.RegisterAsync(user, password);
        }

        public async Task<IdentityResult> ResetPasswordAsync(User user, string code, string password)
        {
            return await _userRepository.ResetPasswordAsync(user, code, password);
        }

        public async Task SignInAsync(User user, bool isPersistent, string authenticationMethod)
        {
            await _userRepository.SignInAsync(user, isPersistent, authenticationMethod);
        }
    }
}