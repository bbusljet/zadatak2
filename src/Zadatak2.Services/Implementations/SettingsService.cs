﻿using Microsoft.Extensions.Options;
using Zadatak2.Models;

namespace Zadatak2.Services
{
    public class SettingsService : ISettingsService
    {
        private ApplicationSettings _applicationSettings;
        private EmailSettings _emailSettings;
        private TwitterSettings _twitterSettings;

        public SettingsService(IOptions<ApplicationSettings> applicationSettings)
        {
            _applicationSettings = applicationSettings.Value;
            _twitterSettings = _applicationSettings.twitterSettings;
            _emailSettings = _applicationSettings.emailSettings;
            
        }

        public string GetConsumerKey()
        {
            return _twitterSettings.consumerKey;
        }

        public string GetConsumerSecret()
        {
            return _twitterSettings.consumerSecret;
        }

        public EmailSettings GetMailSettings()
        {
            return _emailSettings;
        }
    }
}