﻿namespace Zadatak2.Models
{
    public class ApplicationSettings
    {
        public EmailSettings emailSettings { get; set; }
        public TwitterSettings twitterSettings { get; set; }
    }

    public class EmailSettings
    {
        // clas for storing config data for EmailService (MailEnable)
        public string password { get; set; }

        public string smtpHost { get; set; }

        public int smtpPort { get; set; }

        public string userName { get; set; }
    }

    public class TwitterSettings
    {
        public string consumerKey { get; set; }

        public string consumerSecret { get; set; }
    }
}