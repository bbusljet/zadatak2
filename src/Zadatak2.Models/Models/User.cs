﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Zadatak2.Models.Models;

namespace Zadatak2.Models
{
    // The IdentityUser already implements most of the properties i need (User,Email,Password... )
    public class User : IdentityUser
    {
        public UserProfile UserProfile { get; set; }
    }
}