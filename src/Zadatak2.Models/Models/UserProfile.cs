﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Zadatak2.Models.Models
{
    public class UserProfile
    {
        [StringLength(50)]
        public string Adress { get; set; }

        [StringLength(100)]
        public string City { get; set; }

        [StringLength(255)]
        public string Country { get; set; }

        public DateTime? DateofBirth { get; set; }

        [StringLength(255)]
        public string FirstName { get; set; }

        [StringLength(255)]
        public string LastName { get; set; }

        [StringLength(50)]
        public string PostalCode { get; set; }

        [Key, ForeignKey("User")]
        public Guid UserId { get; set; }
    }
}