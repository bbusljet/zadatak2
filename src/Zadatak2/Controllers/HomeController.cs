﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Zadatak2.Services.Interfaces;
using Zadatak2.ViewModel;

namespace Zadatak2.Controllers
{
    public class HomeController : Controller
    {
        private IEmailService _mailService;
        private IUserService _userService;

        public HomeController(IEmailService mailService, IUserService userService)

        {
            _userService = userService;
            _mailService = mailService;
        }

        [Route("Contact")]
        public IActionResult Contact()
        {
            return View();
        }

        [HttpGet]
        public ViewResult ContactConfirmation()
        {
            return View();
        }

        public IActionResult Index()
        {
            return View();
        }

        [Route("Profile")]
        public async Task<IActionResult> Profile()
        {
            if (User.Identity.IsAuthenticated)
            {
                var model = await _userService.GetProfileAsync(User.Identity.Name);
                return View(model);
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Submit(ContactFormViewModel model)
        {
            if (ModelState.IsValid)
            {
                await _mailService.SendContactFormEmailAsync(model.Email, model.Message);

                return View("ContactConfirmation", "Home");
            }
            ModelState.AddModelError("", "Error");
            return View(model);
        }

        [HttpGet]
        [Route("Users")]
        public IActionResult Users()
        {
            var user = _userService.GetRegistered();
            return View(user);
        }
    }
}