﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Zadatak2.Models;
using Zadatak2.Services.Interfaces;
using Zadatak2.ViewModel;

namespace Zadatak2.Controllers
{
    public class AccountController : Controller
    {
        private readonly string _externalCookieScheme;
        private IEmailService _mailService;
        private IUserService _userService;

        public AccountController(IUserService userService, IEmailService mailService, IOptions<IdentityCookieOptions> identityCookieOptions)
        {
            _userService = userService;
            _mailService = mailService;
            _externalCookieScheme = identityCookieOptions.Value.ExternalCookieAuthenticationScheme;
        }

        [HttpPost]
        public IActionResult ExternalLogin(string provider, string returnUrl = null)
        {
            // Request a redirect to the external login provider.
            var redirectUrl = Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl });
            var properties = _userService.ConfigureExternalAuthenticationProperties(provider, redirectUrl, null);
            return Challenge(properties, provider);
        }

        [HttpGet]
        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null)
        {
            var info = await _userService.GetExternalLoginInfoAsync(null);
            if (info == null)
            {
                return RedirectToAction(nameof(Login));
            }

            // Sign in the user with this external login provider if the user already has a login.
            var result = await _userService.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, false);
            if (result.Succeeded)
            {
                return RedirectToLocal(returnUrl);
            }
            else
            {
                // If the user does not have an account, then ask the user to create an account.
                ViewData["ReturnUrl"] = returnUrl;
                ViewData["LoginProvider"] = info.LoginProvider;
                var email = info.Principal.FindFirstValue(ClaimTypes.Email);
                var user = info.Principal.FindFirstValue(ClaimTypes.Name);
                DateTime dateofbirth = Convert.ToDateTime(info.Principal.FindFirstValue(ClaimTypes.DateOfBirth));
                return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = email, UserName = user, DateofBirth = dateofbirth });
            }
        }

        [HttpPost]
        public async Task<IActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await _userService.GetExternalLoginInfoAsync(null);
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }

                User user = Mapper.Map<ExternalLoginConfirmationViewModel, User>(model);

                var result = await _userService.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await _userService.AddLoginAsync(user, info);
                    if (result.Succeeded)
                    {
                        await _userService.SignInAsync(user, false, null);
                        return RedirectToLocal(returnUrl);
                    }
                }
            }

            ViewData["ReturnUrl"] = returnUrl;
            return View(model);
        }

        [HttpGet]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ForgotPassword(ForgorPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = _userService.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    return View("ForgotPasswordConfirmation");
                }
                var code = await _userService.GeneratePasswordTokenAsync(user.Result);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userid = user.Id, code = code }, HttpContext.Request.Scheme);
                await _mailService.SendResetPasswordEmailAsync(model.Email, $"Click here to reset your password: {callbackUrl}");
                return View("ForgotPasswordConfirmation");
            }
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Login()
        {
            await HttpContext.Authentication.SignOutAsync(_externalCookieScheme);
            return View();
        }

        // Login Method
        [HttpPost]
        public async Task<IActionResult> LogIn(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _userService.PasswordSignInAsync(model.UserName, model.Password, false, false);
                if (result.Succeeded) // if the user logged in, bring him to the Index page
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            ModelState.AddModelError("", "Invalid Login");
            return View(model);
        }

        // Logout method
        [HttpPost]
        public async Task<IActionResult> LogOut()
        {
            await _userService.LogoffAsync();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ViewResult Register()
        {
            return View();
        }

        // implementation of Register method
        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid) //check if the user provided all information
            {
                User signInUser = Mapper.Map<RegisterViewModel, User>(model);

                var result = await _userService.RegisterAsync(signInUser, model.Password);
                if (result.Succeeded)
                {
                    //if the result succeded send them an Email
                    await _mailService.SendRegisterEmailAsync(signInUser.Email);
                    await _userService.SignInAsync(signInUser, false, null);
                    return RedirectToAction("Login", "Account");
                }
                else
                {
                    //Result from usermanager has a collection of errors and i can iterate through them
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error.Description); // add errors to modelstate so they are available in View fot TagHelpers
                    }
                }
            }
            return View();
        }

        [HttpGet]
        public ViewResult ResetPassword(string code)
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userService.FindByEmailAsync(model.Email);
                var result = await _userService.ResetPasswordAsync(user, model.Code, model.Password);
                if (result.Succeeded)
                {
                    return RedirectToAction("ResetPasswordConfirmation", "Account");
                }
            }
            return View();
        }

        [HttpGet]
        public IActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }
    }
}