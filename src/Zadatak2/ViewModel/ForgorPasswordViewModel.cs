﻿using System.ComponentModel.DataAnnotations;

namespace Zadatak2.ViewModel
{
    public class ForgorPasswordViewModel
    {
        [Required(ErrorMessage ="Please enter your Email")]
        [EmailAddress]
        [Display(Name ="Email")]
        public string Email { get; set; }
    }
}