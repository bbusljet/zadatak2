﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Zadatak2.ViewModel
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage ="Please enter a User Name")]
        [StringLength(20, MinimumLength = 5)]
        [Display(Name ="User Name")]
        public string UserName { get; set; }

        [Required(ErrorMessage ="Please enter your Email")]
        [EmailAddress]
        [Display(Name ="Email")]
        public string Email { get; set; }

        [Required(ErrorMessage ="Please enter a password"), DataType(DataType.Password)]
        [Display(Name ="Password")]
        [RegularExpression("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$", ErrorMessage ="Your password needs to contain at least 6 characters and at least 1 uppercase and 1 digit")]
        public string Password { get; set; }

        [Required(ErrorMessage ="Confirm password is required"), DataType(DataType.Password)]
        [Display(Name ="Confirm Password")]
        [Compare("Password", ErrorMessage = "The password and confirmation do not match")]
        public string ConfirmPassword { get; set; }

        [StringLength(50)]
        [Display(Name = "Adress")]
        public string Adress { get; set; }

        [StringLength(100)]
        [Display(Name = "City")]
        public string City { get; set; }

        [StringLength(255)]
        [Display(Name = "Country")]
        public string Country { get; set; }

        [Display(Name = "Date of birth")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString ="{0:dd:mm:yyyy}")]
        public DateTime? DateofBirth { get; set; }

        [StringLength(255)]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [StringLength(255)]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [StringLength(50)]
        [Display(Name = "Postal code")]
        public string PostalCode { get; set; }
    }
}