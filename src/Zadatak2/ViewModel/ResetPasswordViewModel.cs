﻿using System.ComponentModel.DataAnnotations;

namespace Zadatak2.ViewModel
{
    public class ResetPasswordViewModel
    {
        [Required(ErrorMessage = "Please enter your Email adress")]
        [EmailAddress]
        [Display(Name ="Email")]
        public string Email { get; set; }

        [Required(ErrorMessage ="Please enter your new password"), DataType(DataType.Password)]
        [Display(Name ="New password")]
        [RegularExpression("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$", ErrorMessage = "Your password needs to contain at least 6 characters and at least 1 uppercase and 1 digit")]
        public string Password { get; set; }

        [Required(ErrorMessage ="Confirm your new password"), DataType(DataType.Password)]
        [Display(Name ="Confirm Password")]
        [Compare("Password", ErrorMessage = "The password and confirmation do not match")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }
}