﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Zadatak2.ViewModel
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required(ErrorMessage ="Please enter your User Name")]
        [Display(Name ="User")]
        public string UserName { get; set; }

        [Required(ErrorMessage ="Please enter your Email")]
        [EmailAddress]
        [Display(Name ="Email")]
        public string Email { get; set; }

        [StringLength(50)]
        [Display(Name = "Adress")]
        public string Adress { get; set; }

        [StringLength(100)]
        [Display(Name = "City")]
        public string City { get; set; }

        [StringLength(255)]
        [Display(Name = "Country")]
        public string Country { get; set; }

        [Display(Name = "Date of birth")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString ="{0:dd:mm:yyyy}")]
        public DateTime? DateofBirth { get; set; }

        [StringLength(255)]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [StringLength(255)]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [StringLength(50)]
        [Display(Name = "Postal code")]
        public string PostalCode { get; set; }
    }
}