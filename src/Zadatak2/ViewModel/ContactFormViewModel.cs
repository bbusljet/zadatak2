﻿using System.ComponentModel.DataAnnotations;

namespace Zadatak2.ViewModel
{
    public class ContactFormViewModel
    {
        [Required(ErrorMessage ="Please enter your user name")]
        [StringLength(20, MinimumLength = 5)]
        [Display(Name ="User Name")]
        public string UserName { get; set; }

        [Required(ErrorMessage ="Please enter your Email")]
        [EmailAddress]
        [Display(Name ="Email")]
        public string Email { get; set; }

        [Required(ErrorMessage ="Please enter your message")]
        [Display(Name ="Message")]
        public string Message { get; set; }
    }
}