﻿using AutoMapper;
using Zadatak2.Models;
using Zadatak2.Models.Models;
using Zadatak2.ViewModel;

namespace Zadatak2.MapperConfiguration
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<RegisterViewModel, User>()
                    .ForMember(x => x.UserProfile, opt => opt.MapFrom(src => new UserProfile
                    {
                        FirstName = src.FirstName,
                        LastName = src.LastName,
                        DateofBirth = src.DateofBirth,
                        Adress = src.Adress,
                        PostalCode = src.PostalCode,
                        City = src.City,
                        Country = src.Country
                    }));

                cfg.CreateMap<ExternalLoginConfirmationViewModel, User>()
                    .ForMember(x => x.UserProfile, opt => opt.MapFrom(src => new UserProfile
                    {
                        FirstName = src.FirstName,
                        LastName = src.LastName,
                        DateofBirth = src.DateofBirth,
                        Adress = src.Adress,
                        PostalCode = src.PostalCode,
                        City = src.City,
                        Country = src.Country
                    }));
            });
        }
    }
}