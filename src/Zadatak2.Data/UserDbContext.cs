﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Zadatak2.Models;
using Zadatak2.Models.Models;

namespace Zadatak2.Data
{
    public class UserDbContext : IdentityDbContext<User>
    {
        public UserDbContext(DbContextOptions options)
           : base(options)
        {
            //this constructor is used to pass options from Startup.cs to the context
        }

        //Defining a property that exposes the User data.
        //With the user property i can query the data from the database.
        public DbSet<User> user { get; set; }
        public DbSet<UserProfile> userProfile { get; set; }
    }
}