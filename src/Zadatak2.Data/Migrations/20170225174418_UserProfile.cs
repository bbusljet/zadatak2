﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Zadatak2.Data.Migrations
{
    public partial class UserProfile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LinkToProfile",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<Guid>(
                name: "UserProfileUserId",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "userProfile",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    Adress = table.Column<string>(maxLength: 50, nullable: true),
                    City = table.Column<string>(maxLength: 100, nullable: true),
                    Country = table.Column<string>(maxLength: 255, nullable: true),
                    DateofBirth = table.Column<DateTime>(nullable: false),
                    FirstName = table.Column<string>(maxLength: 255, nullable: true),
                    LastName = table.Column<string>(maxLength: 255, nullable: true),
                    PostalCode = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_userProfile", x => x.UserId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_UserProfileUserId",
                table: "AspNetUsers",
                column: "UserProfileUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_userProfile_UserProfileUserId",
                table: "AspNetUsers",
                column: "UserProfileUserId",
                principalTable: "userProfile",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_userProfile_UserProfileUserId",
                table: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "userProfile");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_UserProfileUserId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "UserProfileUserId",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<string>(
                name: "LinkToProfile",
                table: "AspNetUsers",
                nullable: true);
        }
    }
}
