﻿using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zadatak2.Data;
using Zadatak2.Models;

namespace Zadatak2.Repository
{
    public class UserRepository : IUserRepository
    {
        private UserDbContext _context;
        private SignInManager<User> _signinmanager;
        private UserManager<User> _usermanager;

        public UserRepository(UserDbContext context, UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _context = context;
            _usermanager = userManager;
            _signinmanager = signInManager;
        }

        public async Task<IdentityResult> AddLoginAsync(User user, UserLoginInfo userLoginInfo)
        {
            return await _usermanager.AddLoginAsync(user, userLoginInfo);
        }

        public AuthenticationProperties ConfigureExternalAuthenticationProperties(string provider, string redirectUrl, string userId)
        {
            var properties = _signinmanager.ConfigureExternalAuthenticationProperties(provider, redirectUrl, userId);
            return properties;
        }

        public async Task<IdentityResult> CreateAsync(User user)
        {
            return await _usermanager.CreateAsync(user);
        }

        public async Task<SignInResult> ExternalLoginSignInAsync(string loginProvider, string providerKey, bool isPersistent)
        {
            return await _signinmanager.ExternalLoginSignInAsync(loginProvider, providerKey, isPersistent);
        }

        public async Task<User> FindByEmailAsync(string email)
        {
            return await _usermanager.FindByEmailAsync(email);
        }

        public async Task<string> GeneratePasswordTokenAsync(User user)
        {
            return await _usermanager.GeneratePasswordResetTokenAsync(user);
        }

        public async Task<ExternalLoginInfo> GetExternalLoginInfoAsync(string expectedXsrf)
        {
            return await _signinmanager.GetExternalLoginInfoAsync(expectedXsrf);
        }

        public async Task<List<User>> GetProfileAsync(string user)
        {
            return await _context.Users.Include(usr => usr.UserProfile).ToListAsync();
        }

        public IEnumerable<User> GetRegistered()
        {
            return _context.Users.ToList();
        }

        public async Task LogoffAsync()
        {
            await _signinmanager.SignOutAsync();
        }

        public async Task<SignInResult> PasswordSignInAsync(string username, string passwordHash, bool isPersistent, bool lockout)
        {
            return await _signinmanager.PasswordSignInAsync(username, passwordHash, false, false);
        }

        public async Task<IdentityResult> RegisterAsync(User user, string password)
        {
            return await _usermanager.CreateAsync(user, password);
        }

        public async Task<IdentityResult> ResetPasswordAsync(User user, string code, string password)
        {
            return await _usermanager.ResetPasswordAsync(user, code, password);
        }

        public async Task SignInAsync(User user, bool isPersistent, string authenticationMethod)
        {
            await _signinmanager.SignInAsync(user, isPersistent, authenticationMethod);
        }
    }
}